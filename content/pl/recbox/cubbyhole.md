---
title: Cubbyhole
date: 2023-03-23T14:37:32+01:00
description: Prosta aplikacja GTK3 napisana w C i używająca rsync do tworzenia kopii zapasowych Twoich nagrań i plików konfiguracyjnych DAW (Digital Audio Workstation) lub czegokolwiek innego. Nie jesteś ograniczony do określonego typu plików.
type: page
topic: recbox
weight: 20
---

![cubbyhole-img](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/cubbyhole.png)

---

## Zależności

- rsync
- GTK 3.24
- glib2 >= 2.74.0

## Instalacja

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-gtk/cubbyhole.git
cd cubbyhole/
make
sudo make install
```

## Usuwanie

```markdown
sudo make clean
```

## Wspierane języki

- [x] en_US
- [x] pl_PL

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-gtk/cubbyhole) | [RecBox Toolkit na forum Manjaro](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
