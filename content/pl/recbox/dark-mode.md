---
title: Dark Mode for XFCE
date: 2023-03-23T14:40:32+01:00
description: Aplikacja Zenity (GTK) napisana w języku Bash do ustawiania jasnych i ciemnych motywów (lub dowolnych innych) globalnie. Obsługuje motywy GTK, Kvantum, Qt, ikony i kursory.
type: page
topic: recbox
weight: 30
---

![dark-mode.gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-darkmode.gif)

---

## Instalacja

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-darkmode.git
cd recbox-darkmode
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Tłumaczenie

Pliki do tłumaczenia można znaleźć w następujących folderach:

- /usr/share/recbox-darkmode/

oraz ikony pulpitu:

- /usr/share/applications/recbox-darkmode.desktop
- /usr/share/applications/recbox-darkmode-settings.desktop

### Wspierane języki

- [x] en_US
- [x] pl_PL

## Pomoc w tłumaczeniu

- w terminalu wpisz `echo $LANG`, aby sprawdzić jakiego języka używa twój system
- skopiuj plik `en_US.trans` z katalogu:
- - /usr/share/recbox-darkmode
- zmień `en_US` na język używany przez system
- zamień tekst między cudzysłowami

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-darkmode) | [RecBox Toolkit na forum Manjaro](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
