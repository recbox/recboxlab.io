---
title: Workflow
date: 2023-03-23T14:33:32+01:00
description: Workflow to narzędzie do ustawiania i przełączania się między ustawieniami domyślnymi i "pro-audio" do nagrywania dźwięku i codziennego użytku.
type: page
topic: recbox
weight: 10
---

![gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-workflow.gif)

---

## Instalacja

```markdown
cd ~
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow.git
cd recbox-workflow
cp -r .config/autostart/* ~/.config/autostart
sudo cp -r etc/* /etc/
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Tłumaczenie

Pliki do tłumaczenia można znaleźć w następujących folderach:

- usr/share/workflow/translations/run
- usr/share/workflow/translations/settings
- /usr/share/workflow/translations/system

oraz ikony pulpitu:

- /usr/share/applications/recbox-workflow-daily-mode.desktop
- /usr/share/applications/recbox-workflow-settings.desktop
- /usr/share/applications/recbox-workflow-studio-mode.desktop

### Wspierane języki

- [x] en_US
- [x] pl_PL

## Pomoc w tłumaczeniu

- w terminalu wpisz `echo $LANG`, aby sprawdzić jakiego języka używa twój system
- skopiuj plik `en_US.trans` z katalogów:
- - usr/share/workflow/translations/run
- - usr/share/workflow/translations/settings
- - /usr/share/workflow/translations/system
- zmień `en_US` na język używany przez system
- zamień tekst między cudzysłowami

```
FREQUENCY_SCALING_TLP_OPTION="Frequency scaling - TLP"
FREQUENCY_SCALING_TLP_DESC="Power Management for Laptops"
FREQUENCY_SCALING_CPU_POWER_OPTION="Frequency scaling - CPU Power"
FREQUENCY_SCALING_CPU_POWER_DESC="Power Management"
```

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-workflow) | [RecBox Toolkit na forum Manjaro](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
