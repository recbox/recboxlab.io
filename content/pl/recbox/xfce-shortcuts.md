---
title: XFCE Shortcuts list
date: 2023-03-23T14:45:32+01:00
description: Lista skrótów dla środowiska graficznego XFCE używającego Zenity (GTK) jako GUI napisanego w Bashu.
type:  page
topic: recbox
weight: 50
---

![image](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-xfce-shortcuts.png)

---

## Instalacja

```markdown
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-xfce-shortcuts.git
cd recbox-xfce-shortcuts
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Tłumaczenie

Pliki do tłumaczenia można znaleźć w następujących folderach:

- /usr/share/xfce-shortcuts/

oraz ikony pulpitu:

- /usr/share/applications/rexbox-xfce-shortcuts.desktop

### Wspierane języki

- [x] de_DE
- [x] en_US
- [x] fr_FR
- [x] pl_PL

## Pomoc w tłumaczeniu

- type `echo $LANG` in terminal to check language used by your system
- copy `en_US.trans` file from:
- - /usr/share/xfce-shortcuts
- rename `en_US` with your system language
- replace text under quotation marks

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-xfce-shortcuts) | [RecBox Toolkit na forum Manjaro](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
