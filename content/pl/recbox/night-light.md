---
title: Night Light
date: 2023-03-23T14:35:32+01:00
description: Aplikacja Zenity (GTK) napisana w języku Bash do korzystania z Redshift w trybie one-shot.
type: page
topic: recbox
weight: 40
---

![gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-nightlight.gif)

---

## Instalacja

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-nightlight.git
cd recbox-nightlight
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity redshift
```
## Tłumaczenie

Pliki do tłumaczenia można znaleźć w następujących folderach:

- /usr/share/recbox-nightlight

oraz ikony pulpitu:

- /usr/share/applications/recbox-nightlight.desktop
- /usr/share/applications/recbox-nightlight-settings.desktop

### Wspierane języki

- [x] en_US
- [x] pl_PL

## Pomoc w tłumaczeniu

- w terminalu wpisz `echo $LANG`, aby sprawdzić jakiego języka używa twój system
- skopiuj plik `en_US.trans` z katalogu:
- - /usr/share/recbox-nightlight
- zmień `en_US` na język używany przez system
- zamień tekst między cudzysłowami

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-nightlight) | [RecBox Toolkit na forum Manjaro](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
