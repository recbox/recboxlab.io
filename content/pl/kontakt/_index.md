---
Title: Kontakt
type: list
---

Jeśli nie ma mnie na żadnej preferowanej przez Ciebie platformie społecznościowej, zawsze możesz napisać do mnie e-mail na adres **projekt.root@proton.me**
