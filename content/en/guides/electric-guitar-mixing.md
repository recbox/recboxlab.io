---
title: Electric Guitar Mixing
date: 2023-03-31T14:37:32+01:00
type: page
topic: guides
weight: 10
---

{{< embed-pdf url="https://player.odycdn.com/api/v4/streams/free/electric-guitar-mixing-on-linux/f1d441a5034a9c7a60156093475b4d84bce8c2f2/e94ca1" >}}

[Preview on Odysee](https://odysee.com/@recbox-recording-music-on-linux:f/electric-guitar-mixing-on-linux:f?r=EoezPFTYVfx9P37y4JJ8cHqze6UA5R5F) | [Direct download link](https://odysee.com/$/download/electric-guitar-mixing-on-linux/f1d441a5034a9c7a60156093475b4d84bce8c2f2)
