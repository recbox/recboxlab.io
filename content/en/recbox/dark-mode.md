---
title: Dark Mode for XFCE
date: 2023-03-23T14:40:32+01:00
description: Zenity (GTK) app written in Bash to set light and dark themes globally (or what ever you like). Supports GTK, Kvantum, Qt, icons and cursors themes.
type: page
topic: recbox
weight: 30
---

![dark-mode.gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-darkmode.gif)

---

## Installation

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-darkmode.git
cd recbox-darkmode
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Translation

Files for Translation can be found here:

- /usr/share/recbox-darkmode/

and desktop icons:

- /usr/share/applications/recbox-darkmode.desktop
- /usr/share/applications/recbox-darkmode-settings.desktop

### Supported languages

- [x] en_US
- [x] pl_PL

## Translation contribution

- type `echo $LANG` in terminal to check language used by your system
- copy `en_US.trans` file from:
- - /usr/share/recbox-darkmode
- rename `en_US` with your system language
- replace text under quotation marks

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-darkmode) | [RecBox Toolkit on Manjaro forum](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
