---
title: Workflow
date: 2023-03-23T14:33:32+01:00
description: Workflow is a utility to set and switch between default and "pro-audio" system settings for recording audio and daily usage.
type: page
topic: recbox
weight: 10
---

![gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-workflow.gif)

---

## Installation

```markdown
cd ~
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-workflow.git
cd recbox-workflow
cp -r .config/autostart/* ~/.config/autostart
sudo cp -r etc/* /etc/
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Translation

Directories containing translations can be found here:

- usr/share/workflow/translations/run
- usr/share/workflow/translations/settings
- /usr/share/workflow/translations/system

and desktop icons:

- /usr/share/applications/recbox-workflow-daily-mode.desktop
- /usr/share/applications/recbox-workflow-settings.desktop
- /usr/share/applications/recbox-workflow-studio-mode.desktop

### Supported languages

- [x] en_US
- [x] pl_PL

## Translation contribution

- type `echo $LANG` in terminal to check language used by your system
- copy `en_US.trans` files from:
- - usr/share/workflow/translations/run
- - usr/share/workflow/translations/settings
- - /usr/share/workflow/translations/system
- rename `en_US` with your system language
- replace text under quotation marks

```
FREQUENCY_SCALING_TLP_OPTION="Frequency scaling - TLP"
FREQUENCY_SCALING_TLP_DESC="Power Management for Laptops"
FREQUENCY_SCALING_CPU_POWER_OPTION="Frequency scaling - CPU Power"
FREQUENCY_SCALING_CPU_POWER_DESC="Power Management"
```

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-workflow) | [RecBox Toolkit on Manjaro forum](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
