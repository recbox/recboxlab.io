---
title: Cubbyhole
date: 2023-03-23T14:37:32+01:00
description: Simple GTK3 app written in C and using rsync to back up your recordings and DAW (Digital Audio Workstation) configuration files, or whatever you like. You're not restricted to a specific type of file.
type: page
topic: recbox
weight: 20
---

![cubbyhole-img](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/cubbyhole.png)

---

## Dependencies

- rsync
- GTK 3.24
- glib2 >= 2.74.0

## Installation

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-gtk/cubbyhole.git
cd cubbyhole/
make
sudo make install
```

## Remove

```markdown
sudo make clean
```

## Supported languages

- [x] en_US
- [x] pl_PL

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-gtk/cubbyhole) | [RecBox Toolkit on Manjaro forum](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
