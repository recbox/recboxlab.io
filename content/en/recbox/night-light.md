---
title: Night Light
date: 2023-03-23T14:35:32+01:00
description: Zenity (GTK) app written in Bash to use Redshift in one-shot mode.
type: page
topic: recbox
weight: 40
---

![gif](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-nightlight.gif)

---

## Installation

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-nightlight.git
cd recbox-nightlight
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity redshift
```
## Translation

Files for Translation can be found here:

- /usr/share/recbox-nightlight/

and desktop icons:

- /usr/share/applications/recbox-nightlight.desktop
- /usr/share/applications/recbox-nightlight-settings.desktop

### Supported languages

- [x] en_US
- [x] pl_PL

## Translation contribution

- type `echo $LANG` in terminal to check language used by your system
- copy `en_US.trans` file from:
- - /usr/share/recbox-nightlight
- rename `en_US` with your system language
- replace text under quotation marks

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-nightlight) | [RecBox Toolkit on Manjaro forum](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
