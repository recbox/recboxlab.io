---
title: XFCE Shortcuts list
date: 2023-03-23T14:45:32+01:00
description: Shortcuts list for XFCE desktop environment using Zenity (GTK) as GUI written in Bash.
type:  page
topic: recbox
weight: 50
---

![image](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-xfce-shortcuts.png "XFCE Shortcuts list")

---

## Installation

### Manjaro

```markdown
git clone https://gitlab.com/recbox/recbox-tools-standalone/recbox-xfce-shortcuts.git
cd recbox-xfce-shortcuts
sudo cp -r sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity
```

## Translation

Files for Translation can be found here:

- /usr/share/xfce-shortcuts/

and desktop icon:

- /usr/share/applications/rexbox-xfce-shortcuts.desktop

### Supported languages

- [x] de_DE
- [x] en_US
- [x] fr_FR
- [x] pl_PL

## Translation contribution
- w terminalu wpisz `echo $LANG`, aby sprawdzić jakiego języka używa twój system
- skopiuj plik `en_US.trans` z katalogu:
- - /usr/share/xfce-shortcuts/
- zmień `en_US` na język używany przez system
- zamień tekst między cudzysłowami

---

[GitLab](https://gitlab.com/recbox/recbox-toolkit-zenity/recbox-xfce-shortcuts) | [RecBox Toolkit on Manjaro forum](https://forum.manjaro.org/t/recbox-toolkit-home-studio/108415)
